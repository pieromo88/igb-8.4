package com.lorainelab.igb.preferences.weblink.view;

/**
 *
 * @author dcnorris
 */
public interface WebLinkDisplayProvider {

    public void displayPanel();

}
